﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TapToChangeScene : MonoBehaviour{
    public GameObject bt;
    // Use this for initialization
    void Start () {
        bt = new GameObject();

}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    
    public void OnInputClicked(){
        switch (this.gameObject.name)
        {
            case ("MenuButton1"):
                SceneManager.LoadScene("Scenes/CalibrationScene", LoadSceneMode.Single);
                Debug.Log("You have clicked the button!");
                break;

            case ("MenuButton2"):
                SceneManager.LoadSceneAsync("Scenes/Grams", LoadSceneMode.Single);
                break;

        }
    }
}
