﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttonpress1 : MonoBehaviour
{
    public void OnInputClicked1()
    {
        SceneManager.LoadScene("Scenes/CalibrationScene", LoadSceneMode.Single);
        Debug.Log("You have clicked the button!");

    }
    public void OnInputClicked2()
    {
        SceneManager.LoadScene("Scenes/Grams", LoadSceneMode.Single);
        Debug.Log("You have clicked the button!");

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
