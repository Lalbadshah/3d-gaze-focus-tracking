﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeMarkerMover : MonoBehaviour {
    public GameObject Curs;
    public PupilSettings Settings;
    public Vector3 Lposition;
    public Vector3 Rposition;
    public LineRenderer laserLineRenderer;
    public float laserWidth = 0.1f;
   int total =0, R = 0, G = 0, B=0;

    // Use this for initialization
    void Start () {
		
	}

    void UpRay(Vector2 LeftPosition)
    {
        Lposition.x = LeftPosition.x;
        Lposition.y = LeftPosition.y;
        Lposition.z = PupilTools.CalibrationType.vectorDepthRadius[0].x;//Default is 2.0f
        Curs.transform.position = PupilSettings.Instance.currentCamera.ViewportToWorldPoint(Lposition);//

        Ray LeyeRay = PupilSettings.Instance.currentCamera.ViewportPointToRay(Lposition);
        RaycastHit raycastHit;
        GameObject hitobject;
        if (Physics.Raycast(LeyeRay,out raycastHit))
        {
            if (raycastHit.collider != null)
            {
                total++;
                hitobject = raycastHit.collider.transform.parent.gameObject;
                switch (hitobject.name)
                {
                    case ("RED"):
                        R++;
                        break;
                    case ("BLUE"):
                        B++;
                        break;
                    case ("GREEN"):
                        G++;
                        break;
                }
            }
        }

        //Vector3 endPosition = Lposition + (20 * PupilSettings.Instance.currentCamera.transform.forward);

        //if (Physics.Raycast(
        //       Lposition,
        //       PupilSettings.Instance.currentCamera.transform.forward,
        //       out raycastHit,
        //       20.0f,
        //       Physics.DefaultRaycastLayers))
        //{

        //    // If the Raycast has succeeded and hit a hologram
        //    // hitInfo's point represents the position being gazed at
        //    // hitInfo's collider GameObject represents the hologram being gazed at

        //    endPosition = raycastHit.point;
        //}
        //laserLineRenderer.SetPosition(0, Lposition);
        //laserLineRenderer.SetPosition(1, endPosition);
        //Debug.DrawRay(Lposition, PupilSettings.Instance.currentCamera.transform.forward, Color.red);
        Debug.Log(Lposition);
    }

    // Update is called once per frame
    void Update () {
        UpRay(PupilData._2D.GetEyeGaze(0));
        //UpRay(PupilData._2D.RightEyePosition);

    }
}
