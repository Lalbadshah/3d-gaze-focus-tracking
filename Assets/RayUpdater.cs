﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayUpdater : MonoBehaviour {


    public PupilSettings Settings;
    public Vector3 Lposition;
    public Vector3 Rposition;
    public float laserWidth = 0.1f;
    public Camera maincam;

    // Use this for initialization
    void Start () {
        //Vector3[] initLaserPositions = new Vector3[2] { Vector3.zero, Vector3.zero };
        //laserLineRenderer.SetPositions(initLaserPositions);
        //laserLineRenderer.SetWidth(laserWidth, laserWidth);
        PupilSettings.Instance.currentCamera = maincam;


    }

    void UpRay(Vector2 LeftPosition)
    {
        Lposition.x = LeftPosition.x;
        Lposition.y = LeftPosition.y;
        Lposition.z = PupilTools.CalibrationType.vectorDepthRadius[0].x;
        gameObject.transform.position = PupilSettings.Instance.currentCamera.ViewportToWorldPoint(Lposition);//

        Ray LeyeRay = PupilSettings.Instance.currentCamera.ViewportPointToRay(Lposition);
        //RaycastHit raycastHit;

        //Vector3 endPosition = Lposition + (20 * PupilSettings.Instance.currentCamera.transform.forward);

        //if (Physics.Raycast(
        //       Lposition,
        //       PupilSettings.Instance.currentCamera.transform.forward,
        //       out raycastHit,
        //       20.0f,
        //       Physics.DefaultRaycastLayers))
        //{

        //    // If the Raycast has succeeded and hit a hologram
        //    // hitInfo's point represents the position being gazed at
        //    // hitInfo's collider GameObject represents the hologram being gazed at

        //    endPosition = raycastHit.point;
        //}
        //laserLineRenderer.SetPosition(0, Lposition);
        //laserLineRenderer.SetPosition(1, endPosition);
        Debug.DrawRay(Lposition, PupilSettings.Instance.currentCamera.transform.forward,Color.red);
        Debug.Log(Lposition);
    }
	
	// Update is called once per frame
	void Update () {

        UpRay(PupilData._2D.LeftEyePosition);
        //UpRay(PupilData._2D.RightEyePosition);


    }
}
