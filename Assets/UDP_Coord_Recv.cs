﻿using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using System.Linq;



public class UDP_Coord_Recv : MonoBehaviour {

    

    static int total = 0, R = 0, G = 0, B = 0;
    private Thread thread = null;
    public Camera Camera;
    public Cursor Cursor;
    public GameObject Inputmanager;
    public GameObject ttp;
    public int port;
    public IPAddress IPAddress;
    public GameObject Spat;
    public GameObject sph;
    private Vector3 position;
    public int mins;
    RaycastHit raycastHit;
    GameObject hitobject;
    bool timetodie;
    DateTime start;

    void OnEnable()
    {

        Debug.Log("PrintOnEnable: udp script was enabled");
        MonoBehaviour[] scriptComponents = ttp.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scriptComponents)
        {
            script.enabled = false;
        }
        MonoBehaviour[] scripts = Inputmanager.GetComponents<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = false;
        }
        Inputmanager.SetActive(false);
        Spat.SetActive(false);
        sph.SetActive(true);

        start = DateTime.Now;
        

        UdpNetworkListenManager(port);
    }

    public void UdpNetworkListenManager(int port)
    {
        hitobject = new GameObject();

        thread = new Thread(() => {
            Debug.Log("Thread Started");
            UdpClient udpclient = new UdpClient(port);
            IPEndPoint remote = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            try
            {

                while (true){
                    byte[] bytes = udpclient.Receive(ref remote);
                    string ms = Encoding.UTF8.GetString(bytes);//Recceived Coordinates

                    //split by comma to get x and y Z is 2.0f default
                    float[] newPosCoordinates = ms.Split(new string[] { "," }, StringSplitOptions.None).Select(x => float.Parse(x)).ToArray();

                    position = new Vector3(newPosCoordinates[0], newPosCoordinates[1], 2.0f);

                    //position is a 3vector x,y,z,
                    //this.Update(position);
                }



            }

            catch (Exception) { }
            udpclient.Close();
        });
        thread.Start();
    }

    
    private void Update()
    {
        if ((DateTime.Now-start).TotalMinutes >= mins)
        {
            Debug.Log("Stopping Execution");
            float red = R / total;
            float green = G / total;
            float blue = B / total;
            float[] colrs = { red,green,blue};
            float max3 = Math.Max(red, Math.Max(green, blue));
            int indx = Array.IndexOf(colrs, max3);
            switch (indx)
            {
                case 0:
                    Debug.Log("Red Cube was gazed at the most: " + red * 100 + "% of the total time");
                    break;

                case 1:
                    Debug.Log("Green Cube was gazed at the most: " + green * 100 + "% of the total time");
                    break;

                case 2:
                    Debug.Log("Blue Cube was gazed at the most: " + blue * 100 + "% of the total time");
                    break;
            }
            Debug.Log("Registered Hits: RED-"+R+" GREEN-"+G+" BLUE-"+B);
            return;
        }
        sph.transform.position = Camera.ViewportToWorldPoint(position);
        

        Ray LeyeRay = Camera.ViewportPointToRay(position);
        
        if (Physics.Raycast(LeyeRay, out raycastHit))
        {
            if (raycastHit.collider != null)
            {
                total++;
                hitobject = raycastHit.collider.transform.parent.gameObject;
                switch (hitobject.name)
                {
                    case ("RED"):
                        R++;
                        Debug.Log("HIT RED");
                        break;
                    case ("BLUE"):
                        B++;
                        Debug.Log("HIT BLUE");
                        break;
                    case ("GREEN"):
                        G++;
                        Debug.Log("HIT GREEN");
                        break;
                }
            }
        }
        Debug.Log(position);
        
    }

}