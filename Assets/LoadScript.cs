﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SceneManager.LoadSceneAsync("Scenes/Calib", LoadSceneMode.Single);	
	}
	
	//// Update is called once per frame
	//void Update () {
		
	//}
}
